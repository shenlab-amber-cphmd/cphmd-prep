# CpHMD Preparation/Analysis Tools

 A set of tools to help prepare and run
 **C**ontinuous Constant **pH** **M**olecular
 **D**ynamics (**CpHMD**) simulation outputs. CpHMD is developed by
 [Dr. Jana Shen's group](https://www.computchem.org/) at the
 University of Maryland School of Pharmacy.

 The processParm7 module can be used to change the GB radii
 of some of the atoms in your Amber parm7 file to the
 values required in CpHMD simulations.

## Requirements

* Python ==3.6
* Scipy >=1.3
* ParmEd >=3.2
* OpenMM >=7.3
* pdbfixer >=1.6
* AmberTools >=18.0

## Installation

 If you use Git, choose a directory where you want to store the files, and
 run the following commands to install the library into your python
 installation,

   `git clone https://gitlab.com/shenlab-amber-cphmd/cphmd-prep`

   `cd cphmd-prep`

   `python setup.py install`

 Alternatively, you can download the zip/tarball directly. If you download
 the zip/tarball file, unzip/untar it first. For example, if you download
 a `.zip` file:

   `unzip cphmd_tools-master.zip -d cphmd_tools`

   `cd cphmd_tools`

   `python setup.py install`

 **Note 1: It is a good idea to create a python [virtual environment](https://docs.python.org/3/tutorial/venv.html)
   first to avoid conflicts with your other python projects.

**Note 2: You can also install `ParmEd`, `OpenMM`, `pdbfixer`, and `scipy` manually through `pip` or `conda`.

    `conda create --name cphmd-prep python=3.6`
    `conda activate cphmd-prep`
    `conda install -c omnia parmed`
    `conda install -c conda-forge ambertools pdbfixer`

## Usage

1. Either add the `cphmd_prep` directory to your `PATH` by `export PATH=$PATH:your_directory/cphmd_pred` or copy the `amber_cphmd.sh` file in the `cphmd_tools` folder to an existing foler already in your `PATH`.

2. Set up environmental variables `AMBERHOME` and `CUDA_VISIBLE_DEVICES`. `AMBERHOME` is the directory for the working Amber version and `pmemd.cuda` should be in the `AMBERHOME/bin` directory. `CUDA_VISIBLE_DEVICES` indicates the Nvidia GPU ID(s) that you want to use for CpHMD simulations. For example, `export CUDA_VISIBLE_DEVICES=0` will assign the GPU card with id=0 to be used.

3. Under any directory, you can type `amber_cphmd.sh -p 1vii -l 1 -pmin 7.0 -pmax 7.5` for a quick test, and the output files are in the `1vii` subdirectory. For more control parameters, type `amber_cphmd.sh -h` to see options.

To better understand pKa's calculated by CpHMD, check our recent related papers like [spotting protein kinase covalent inhibitor targets](https://pubs.acs.org/doi/10.1021/jacs.8b13248), [pH-responsive material design](https://pubs.acs.org/doi/full/10.1021/acs.chemmater.8b03753) and [GPU accelerated CpHMD](https://pubs.acs.org/doi/10.1021/acs.jcim.9b00754). Our [tutorial](http://wiki.computchem.org/en/tutorials/GBNeck2-CpHMD) is a good place to check the detailed step-by-step setups. 

If you use the tools in this repositoy, please cite:

Ruibin Liu, Zhi Yue, Cheng-Chieh Tsai, and Jana Shen, *Assessing Lysine and Cysteine Reactivities for Designing Targeted Covalent Kinase Inhibitors*, *J. Am. Chem. Soc.* 2019, 141, 16, 6553–6560

