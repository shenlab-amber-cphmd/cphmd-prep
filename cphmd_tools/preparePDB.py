#!/usr/bin/env python
from pdbFixer import phmdPDBPreparer
from pdbSplt import splitPDB
from pdbSlct import selectModel
import argparse
import os.path

def prepare():
    parser = argparse.ArgumentParser(description="""Full processes to prepare an initial PDB structure
                                    that can be read by tleap with CpHMD parameter sets.
                                    The final PDB file is named '*fixed.pdb'.""")
    parser.add_argument('-f', '--pdbfile', default=None, dest='pdbfile', metavar='PDBFILE',
                        help='An input standard PDB file [default: None]')
    parser.add_argument('-p', '--pdbid', default=None, dest='pdbid', metavar='PDBID',
                        help='PDB id to retrieve from RCSB [default: None]')
    parser.add_argument('-c', '--chainid', default='A', dest='chainid', metavar='CHAINID',
                        help="Chain id: the single letter chain label in a PDB file  [default: 'A']")
    parser.add_argument('-m', '--modelid', default='-', dest='modelid', metavar='MODELID',
                        help="Model id: can be either an alternative model or a different modeling [default: '-']")
    parser.add_argument('-mu', '--mutate', default=False, dest='mutate', metavar='MUTATE',
                        help="Mutate pattern: perform mutation on certain residues [default: False]")
    parser.add_argument('-type', '--residueTypes', default="GLU ASP HIS CYS LYS", dest='residueTypes', metavar='',
                        help="Titratable residue types [default: 'GLU ASP HIS CYS LYS']")
    args = parser.parse_args()
    # if len(args.modelid) != 1:
    #     parser.error('Please specify only one model!')
    if args.pdbid is None and args.pdbfile is None:
        parser.error('No filename or PDB ID specified')
    elif args.pdbid is None and args.pdbfile is not None:
        if '.pdb' in args.pdbfile:
            identifier = args.pdbfile[0:-4]
        else:
            identifier = args.pdbfile
        if args.modelid == '-':
            fileName = args.pdbfile
            phmdPDBPreparer(pdbFile=fileName, chainID=args.chainid.split(' '), mutate=args.mutate, residueTypes=args.residueTypes.split())
            return
        elif args.modelid.isnumeric():
            splitPDB(fileName=args.pdbfile)
        else:
            selectModel(fileName=args.pdbfile, modelID=args.modelid)
    elif args.pdbid is not None and args.pdbfile is None:
        identifier = args.pdbid
        if args.modelid == '-':
            phmdPDBPreparer(pdbID=identifier, chainID=args.chainid.split(' '), mutate=args.mutate, residueTypes=args.residueTypes.split())
            return
        elif args.modelid.isnumeric():
            splitPDB(pdbID=identifier)
        else:
            selectModel(pdbID=identifier, modelID=args.modelid)
    else:
        parser.error('Specify either filename or PDB ID, not both')
    fileName = '{}_{}.pdb'.format(identifier, args.modelid)
    if os.path.exists(fileName):
        phmdPDBPreparer(pdbFile=fileName, chainID=args.chainid.split(' '), mutate=args.mutate, residueTypes=args.residueTypes.split())


if __name__ == '__main__':
    prepare()
