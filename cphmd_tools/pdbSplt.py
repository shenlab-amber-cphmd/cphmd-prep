#!/usr/bin/env python
import urllib.request

def splitPDB(fileName='', pdbID=False, keepHeader=True):
    """Split PDB file into separate files corresponding to different models

    Separate different models from the provided PDB file or downloaded PDB file if only pdbID is provided.
    :param fileName: PDB file name
    :type fileName: str
    :param pdbID: PDB ID; if fileName is not provided, a PDB file corresponding to it will be downloaded from RCSB.
    :type pdbID: str
    :param keepHeader: If true, all PDB header information will be kept in each separated PDB file.
    :type keepHeader: bool

    :returns: PDB file names for the separated models.
    :rtype: list of str
    """
    if not (bool(pdbID) or bool(fileName)):
        raise ValueError("No PDB file or PDB id is provided.")
    identifier = pdbID
    if bool(fileName) and not bool(pdbID):
        identifier = fileName.split('.')[0]
    else:
        url = 'http://www.rcsb.org/pdb/files/{}.pdb'.format(pdbID)
        fileName = '{}.pdb'.format(pdbID)
        try:
            urllib.request.urlretrieve(url, fileName)
        except Exception:
            print("Cannot download PDB file '{}' from RCSB.".format(fileName))
            exit()
    header = []  # Header lines
    models = []  # Lines for different models
    numModel = 0
    modelFileNames = []
    with open(fileName, 'r') as of:
        for line in of:
            if line[0:5] == 'MODEL':
                numModel += 1
                models.append([])
                modelFileNames.append(identifier + '_{}'.format(numModel) + '.pdb')
                while line[0:6] != 'ENDMDL':
                    models[-1].append(line)
                    line = of.readline()
            else:
                header.append(line)
    for i in range(numModel):
        with open(modelFileNames[i], 'w') as mf:
            # for hl in header[0:-2]:
            #     mf.write(hl)
            for ml in models[i]:
                mf.write(ml)
    return modelFileNames


if __name__ == "__main__":
    import sys
    if len(sys.argv) < 2:
        raise Exception('Need to provide a PDBfile or PDB id.')
    elif len(sys.argv) == 2:
        inputString = sys.argv[1]
    if len(inputString) == 4:
        try:
            splitPDB(pdbID=inputString)
        except urllib.error.HTTPError:
            splitPDB(fileName=inputString)
    else:
        splitPDB(fileName=inputString)

