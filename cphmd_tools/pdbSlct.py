#!/usr/bin/env python
import urllib.request

def selectModel(fileName='', pdbID=False, modelID='A'):
    """Select the part belongs to a specific model in a PDB file

    Only coordinates belong to a provided model ID are saved from the provided PDB file or
    the downloaded PDB file corresponding to the provided PDB ID.
    :param fileName: PDB file name
    :type fileName: str
    :param pdbID: PDB ID; if fileName is not provided, a PDB file corresponding to it will be downloaded from RCSB.
    :type pdbID: str
    :param modelID: The model ID to select.
    :type modelID: str

    :returns: PDB file name for the selected model.
    :rtype: str
    """
    if not (bool(pdbID) or bool(fileName)):
        raise ValueError("No PDB file or PDB id is provided.")
    identifier = pdbID
    if bool(fileName) and not bool(pdbID):
        identifier = fileName.split('.')[0]
    else:
        url = 'http://www.rcsb.org/pdb/files/{}.pdb'.format(pdbID)
        fileName = '{}.pdb'.format(pdbID)
        try:
            urllib.request.urlretrieve(url, fileName)
        except Exception:
            print("Cannot download PDB file '{}' from RCSB.".format(fileName))
            exit()
    outFile = '{}_{}.pdb'.format(identifier, modelID)
    with open(fileName, 'r') as of, open(outFile, 'w') as nf:
        for line in of:
            if line[0:6] in ['HETATM', 'ANISOU', 'ATOM  '] and (line[16] == modelID or line[16] == ' '):
                line = list(line)
                line[16] = ' '  # To eliminate this info
                line = ''.join(line)
                nf.write(line)
            elif line[0:6] not in ['HETATM', 'ANISOU', 'ATOM  ']:
                nf.write(line)
    return outFile  # the output file of the selected model


if __name__ == "__main__":
    import sys
    if len(sys.argv) < 2:
        raise Exception('Need to provide a PDBfile or PDB id.')
    elif len(sys.argv) == 2:
        inputString = sys.argv[1]
        modelID = 'A'
    elif len(sys.argv) >= 3:
        inputString = sys.argv[1]
        modelID = sys.argv[2]
    if len(inputString) == 4:
        try:
            selectModel(pdbID=inputString, modelID=modelID)
        except urllib.error.HTTPError:
            selectModel(fileName=inputString, modelID=modelID)
    else:
        selectModel(fileName=inputString, modelID=modelID)

