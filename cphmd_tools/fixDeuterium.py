#!/usr/bin/env python
import urllib.request
import os.path
import os


def fixDeuterium(pdbID='', pdbFile='', outFile=''):
    """Replace Deuterium with Hydrogen

    This function replaces Deuterium atoms with Hydrogen atoms in PDB files.
    :param pdbID: String, a four-letter PDB identifier from RCSB
    :param pdbFile: String, standard PDB file
    :return A fixed PDB file
    """
    if bool(pdbID):
        url = 'http://www.rcsb.org/pdb/files/{}.pdb'.format(pdbID)
        pdbFile = '{}.pdb'.format(pdbID)
    if not os.path.exists(pdbFile):
        urllib.request.urlretrieve(url, pdbFile)
    # if '.pdb' in pdbFile:
    #     identifier = pdbFile[0:-4]
    if outFile == '':
        tmpFile = 'tmp_' + pdbFile
    else:
        tmpFile = outFile

    with open(pdbFile, 'r') as ipf, open(tmpFile, 'w') as tpf:
        for line in ipf:
            line = list(line)
            if line[77] == 'D':
                for i, c in enumerate(line[12:16]):
                    if c == 'D':
                        line[i+12] = 'H'
                        break
                line[77] = 'H'
            line = ''.join(line)
            tpf.write(line)
    if tmpFile != outFile:
        os.rename(tmpFile, pdbFile)


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description="""Full processes to prepare an initial PDB structure
                                                    that doesn't need to select models and can be read
                                                    by tleap with CpHMD parameter sets. The final PDB
                                                    file is named '*fixed.pdb'.""")
    parser.add_argument('-f', '--pdbfile', default='', dest='pdbfile', metavar='PDBFILE',
                        help='An input standard PDB file [default: '']')
    parser.add_argument('-p', '--pdbid', default='', dest='pdbid', metavar='PDBID',
                        help='PDB id to retrieve from RCSB [default: '']')
    parser.add_argument('-o', '--outfile', default='', dest='outfile', metavar='OUTFILE',
                        help='An output standard PDB file [default: '']')
    args = parser.parse_args()
    fixDeuterium(pdbID=args.pdbid, pdbFile=args.pdbfile, outFile=args.outfile)
