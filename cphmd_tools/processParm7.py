#!/usr/bin/env python
from parmed.tools import addExclusions
from parmed.amber import AmberParm
from pathlib import Path


def processParm7(pdbFile='', parm7File='', newParm7File=''):
    '''Process original Amber .parm7 file

    This program processes tleap originally-generated .parm7 files for CpHMD usage, including adjusting GB radii
    and adding exclusions between dummy hydrogens.

    :param pdbFile: String, the PDB file saved along with the original .parm7 file by tleap, default is ''.
    :param parm7File: String, the original .parm7 file, default is ''.
    :param newParm7File: String, the original .parm7 file, default is ''.
    :return True/False if successful/failed.
    '''
    # Inputs
    if pdbFile == '' or parm7File == '':
        raise Exception('InputError: Not enough input files.')
    else:
        parm7Dir = Path(parm7File).parent
        parm7Name = Path(parm7File).stem
        if newParm7File == '':
            newParm7File = "{}/cphmd_{}.parm7".format(parm7Dir, parm7Name)

    # Read pdb file and generate (HD1, HD2) or (HE1, HE2) pair lists
    atomType = []    # atom type for each atom
    resType = []     # residue type for each atom
    atomIndex1 = []  # atom indices for HD1/HE1
    atomIndex2 = []  # atom indices for HD2/HE2
    with open(pdbFile, 'r') as f:
        for line in f:
            if line[0:4] == 'ATOM':
                atomType.append(line[11:17].strip())
                resType.append(line[17:20])
                if line[17:20] == 'AS2' or line[17:20] == 'GL2':
                    if line[11:17].strip() == 'HD1' or line[11:17].strip() == 'HE1':
                        atomIndex1.append(int(line[4:11].strip()))
                    elif line[11:17].strip() == 'HD2' or line[11:17].strip() == 'HE2':
                        atomIndex2.append(int(line[4:11].strip()))
    if len(atomIndex1) != len(atomIndex2):
        raise Exception('Wrongly built PDB file.')

    # Change the radii of SG atom in Cys and HZ* atoms in LYS in the parm7 file
    tmpFile = 'tmp.parm7'   # A temprary file will be generated in the working directory
    with open(parm7File, 'r') as f1, open(tmpFile, 'w+') as f2:
        for line in f1:
            if len(line.split()) > 1:
                if line.split()[1] != 'RADII':
                    f2.write(line)
                else:
                    f2.write(line)
                    line = f1.readline()
                    f2.write(line)
                    n = 0
                    line = f1.readline()
                    while line.split()[0] != '%FLAG':
                        n += 1
                        for i in range(len(line.split())):
                            if resType[(n-1)*5 + i] == 'CYS' and atomType[(n-1)*5 + i] == 'SG':
                                f2.write('  2.00000000E+00')  # For SG atom of CYS, the original values is 1.8
                            # elif resType[(n-1)*5 + i] == 'LYS' and (atomType[(n-1)*5 + i] == 'HZ1'
                            #                                         or atomType[(n-1)*5 + i] == 'HZ2'
                            #                                         or atomType[(n-1)*5 + i] == 'HZ3'):
                            #     f2.write('  1.17000000E+00')  # For HZ1, HZ2, and HZ3 atoms of LYS
                            # This change is not used anymore.
                            elif resType[(n-1)*5 + i] == 'HIP' and (atomType[(n-1)*5 + i] == 'HD1'
                                                                    or atomType[(n-1)*5 + i] == 'HE2'):
                                f2.write('  1.17000000E+00')  # For HD1 and HE2 atoms of HIP, the original value is 1.3
                            # elif resType[(n-1)*5 + i] == 'AS2' and (atomType[(n-1)*5 + i] == 'OD1'
                            #                                         or atomType[(n-1)*5 + i] == 'OD2'):
                            #     f2.write('  1.40000000E+00')  # For OD1 and OD2 atoms of AS2
                            # elif resType[(n-1)*5 + i] == 'GL2' and (atomType[(n-1)*5 + i] == 'OE1'
                            #                                         or atomType[(n-1)*5 + i] == 'OE2'):
                            #     f2.write('  1.40000000E+00')  # For OE1 and OE2 atoms of GL2
                            # The above two changes do nothing in mbondi3, commented out for clarity.
                            else:
                                f2.write('  ' + line.split()[i])
                        f2.write('\n')
                        line = f1.readline()
                    f2.write(line)
            else:
                f2.write(line)

    # Add HD1-HD2 of AS2 and HE1-HE2 GL2 pairs into exclusion list
    amberParm7 = AmberParm(tmpFile)
    for id1, id2 in zip(atomIndex1, atomIndex2):
        select1 = '@{}'.format(id1)
        select2 = '@{}'.format(id2)
        action = addExclusions(amberParm7, select1, select2)
        action.execute()

    amberParm7.save(newParm7File, overwrite=True)
    # print("Processed parm7 file is saved as '{}'.".format(newParm7File))
    tp = Path('tmp.parm7')
    tp.unlink()  # The temprary file is deleted from the working directory
    return newParm7File


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description="""A Python program to process tleap originally-generated .parm7 files
                                    for CpHMD usage, including adjusting GB radii and adding exclusions
                                    between dummy hydrogens.""")
    parser.add_argument('-f', '--pdbfile', default='', dest='pdbfile', metavar='PDBFILE',
                        help='An input PDB file along with the .parm7 file. [default: '']')
    parser.add_argument('-p', '--parm7file', default='', dest='parm7file', metavar='PARM7FILE',
                        help='The Amber parm7 file to process. [default: '']')
    parser.add_argument('-o', '--newparm7file', default='', dest='newparm7file', metavar='NEWPARM7FILE',
                        help="""The processed Amber parm7 file. [default: '']
                            in which case a new file with suffix 'cphmd_' will be used.""")
    args = parser.parse_args()
    processParm7(pdbFile=args.pdbfile, parm7File=args.parm7file, newParm7File=args.newparm7file)
