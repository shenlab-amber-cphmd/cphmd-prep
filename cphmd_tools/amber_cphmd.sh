#! /bin/bash

##################################################################
# Prepare, run, and analyze CpHMD simulations                 #
##################################################################


####################################
# Take care of inputs              #
####################################

# Default job settings
me=`basename "$0"`                      # This script
POSITIONAL=()                           # For parsing command line arguments
PDBID=''                                # PDB ID for which a PDB file can be downloaded from RCSB
PDBFILE=''                              # PDB file name 
CHAINID='A'                             # Default chain selection
MODELID=''                              # No alternative model in default PDB files
istest=false                            # If true, tepmorary files are not deleted
outfiletype='zip'                       # Output files will be in a 'zip' file or a 'tar.gz' file depending on input. 
pHmin=6.5                               # Default minimum pH value
pHmax=9.0                               # Default maximum pH value
pHintvl=0.5                             # Default pH interval
ionic=0.15                              # Default ionic strength 0.15M
simlength=10                            # Default simulation length 10ns
temperature=300                         # Default simulation temperature 300K
residuetypes='Asp Glu His Cys Lys'      # Default titratable residues
mode='async'                            # CpHMD run mode: ['async', 'reex', 'ind']

# Parse input arguments
usage()
{
echo "    Usage: $me [[-p|--pdbid]|[-f|--pdbfile]] [-c|--chainid]" \
    "[-m|--modelid] [-t|--test]" \
    "[-i|--ionic] [-ot|--outfiletype]" \
    "[-l|--simlength] [-tp|--temperature] [-pi|--pHintvl]" \
    "[-pmin|--pHmin] [-pmax|--pHmax]" \
    "[-type|--residuetypes] [-rm|--runmode] [-h|--help]" \

cat <<EOF

    -h|--help               This help message.
    -p|--pdbid              PDB ID to retrieve the corresponding PDB file from RCSB; default is ''.
    -f|--pdbfile            PDB file name; default is ''.
                            Either a PDB ID or a PDB file name is required.
    -c|--chainid            Chain IDs to include in simulations; default is 'A'.
    -m|--modelid            The model ID for crystal structures refined from modeling; default is ''.
    -t|--test               If true, all intermediate files are saved for debugging; default is false.
    -ot|--outfiletype       Output files will be in a 'zip' file or a 'tar.gz' file depending on input; default is 'zip'.
    -i|--ionic              Ionic strength in M used in the GBNeck2 implict solvent model; default is 0.15M.
    -l|--simlength          Simulation length in nano second for each pH condition; default is 10.
    -tp|--temperature       Simulation temperature in Kelvin; default is 300.
    -pi|--pHintvl           pH interval between lowest and highest pH conditions; default is 0.5.
    -pmin|--pHmin           Minimum pH condition for CpHMD simulaitons; default is 6.5.
    -pmax|--pHmax           Maximum pH condition for CpHMD simulations; default is 9.0.
    -type|--residuetypes    Amino acid residue types set to be titratable in CpHMD simulations;
                            default is 'Asp Glu His Cys Lys'.
    -rm|--runmode            CpHMD run mode. 'async': use asynchronous pH replica exchange on GPU(s); 'reex': use classical pH replica exchange on CPUs; 'ind': use independent pH simulations on GPU(s)
EOF
}
while [[ $# -gt 0 ]]
do
key="$1"
case $key in
    -p|--pdbid)
    PDBID="$2"
    shift # past argument
    shift # past value
    ;;
    -f|--pdbfile)
    PDBFILE="$2"
    shift # past argument
    shift # past value
    ;;
    -c|--chainid)
    CHAINID="$2"
    shift # past argument
    shift # past value
    ;;
    -m|--modelid)
    MODELID="$2"
    shift # past argument
    shift # past value
    ;;
    -i|--ionic)
    ionic="$2"
    shift # past argument
    shift # past value
    ;;
    -l|--simlength)
    simlength="$2"
    shift # past argument
    shift # past value
    ;;
    -tp|--temperature)
    temperature="$2"
    shift # past argument
    shift # past value
    ;;
    -pi|--pHintvl)
    pHintvl="$2"
    shift # past argument
    shift # past value
    ;;
    -pmin|--pHmin)
    pHmin="$2"
    shift # past argument
    shift # past value
    ;;
    -pmax|--pHmax)
    pHmax="$2"
    shift # past argument
    shift # past value
    ;;
    -type|--residuetypes)
    residuetypes="$2"
    shift # past argument
    shift # past value
    ;;
    -ot|--outfiletype)
    outfiletype="$2"
    shift # past argument
    shift # past argument
    ;;
    -h|--help)
    usage
    exit  
    shift # past value
    ;;
    -t|--test)
    istest=true
    shift # past argument
    ;;    
    -b|--build)
    buildonly=true
    shift # past argument
    ;;
    -rm|--runmode)
    mode="$2"
    shift # past argument
    shift # past value
    ;;
    *)    # unknown option
    usage
    exit  
    POSITIONAL+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters


#########################
# Take care of inputs   #
#########################

# Determine this package installation path and initiate system path
cphmd="`dirname \"$0\"`"                        # this script
cphmd="`( cd \"$cphmd\" && cd .. && pwd )`"     # cphmd package directory
if [ -z "$cphmd" ]; then
    echo "Error: the $cphmd directory is not accessible."
    exit 1 
fi
export PYTHONPATH=$PYTHONPATH:$cphmd/cphmd_tools
export PATH=$PATH:$cphmd/cphmd_tools
if [[ $AMBERHOME ]]
then
    amberbin="$AMBERHOME/bin"
else
    echo "AMBERHOME is not set but required for using tleap."
    exit
fi

echo 'PATH set' > job_status.log

# Conditions used in thre prepared CpHMD calculations 
if [ "$PDBID" == '' ] && [ "$PDBFILE" == '' ]; then
	echo "Error: must provide a PDB id or a PDB file."
	usage
	exit 1
elif [ "$PDBID" != '' ] && [  "$PDBFILE" == '' ]; then
	echo "Provided PDB ID: ${PDBID}" > job_info.out
	dirname=$PDBID
    PDB_ID=$PDBID
elif [ "$PDBID" == '' ] && [ "$PDBFILE" != '' ]; then
    if [ -f "$PDBFILE" ]; then
        echo "Provided PDB file: ${PDBFILE}" > job_info.out
    else
        echo "PDB file $PDBFILE not found in the working directory." 
        exit 1
    fi
	PDB_FILE=`basename "$PDBFILE"`
    PDB_ID=$(echo $PDB_FILE | cut -c1-4)  # Use the first four letters as PDB ID in use
	dirname=$(echo ${PDB_FILE%.pdb})
else
	echo "Error: must provide either a PDB id or a PDB file, not both."
	usage
	exit 1
fi

echo "Chain ID: ${CHAINID}" >> job_info.out 

if [ "$MODELID" != '' ]; then
	echo "Model ID: ${MODELID}" >> job_info.out 
else
	MODELID='-'
fi
echo "Titratable residue types: $residuetypes" >> job_info.out

pHs=()
pH=$(echo "scale=2; $pHmin * 1 / 1" | bc)   # always float
while [ $(echo "$pH<=$pHmax" | bc) == 1 ]
do
    pHs+=($pH)
    pH=$(echo "scale=2; ($pH + $pHintvl) * 1 / 1" | bc)
done
echo "pH conditions for CpHMD: "${pHs[@]}", calculated from min pH $pHmin, max pH $pHmax, and interval $pHintvl." >> job_info.out
echo "Temperature for MD simulations: ${temperature}K" >> job_info.out 
echo "Implicit salt concentration: ${ionic}M" >> job_info.out

echo "Target simulation length for each pH: ${simlength}ns" >> job_info.out

# Check other output control parameters
if [ "$istest" = true ]; then
	echo "This is a test job. Every file will be kept." >> job_info.out
else
    echo "Temporary files are deleted." >> job_info.out
fi

if [ "$mode" == 'ind' ]; then
    echo "Use independent pH CpHMD simulations" >> job_info.out
elif [ "$mode" == 'reex' ]; then
    echo "Use classical pH replica exchange CpHMD simulations" >> job_info.out
elif [ "$mode" == 'async' ]; then
    echo "Use asynchronous pH replica exchange CpHMD simulations" >> job_info.out
else
    echo "Mode $mode is not supported." >> job_log
    exit 1
fi

if [ "$outfiletype" != 'zip' ] && [ "$outfiletype" != 'tar.gz' ]; then
    echo "Only 'zip' and 'tar.gz' file types are supported for the final prepared file folder."
    exit 1
fi

echo 'Inputs processed' > job_status.log

#######################################
# prepared CpHMD ready PDB file #
#######################################
currentdir=`pwd`
py3dir=`which python3`
if [ -z $py3dir ]; then
    echo "Error: python3 is not installed."
    exit 1
else
    [[ -d $dirname ]] || mkdir $dirname
    cd $dirname
    if [ "$PDBID" != '' ]; then
        preparePDB.py -p $PDBID -c "$CHAINID" -m $MODELID -type "$residuetypes" > prepare.log
        wait
        fixedpdb=`ls ${PDBID}*_fixed.pdb`
        if [ -f "$fixedpdb" ]; then
            echo "CpHMD PDB file: ${fixedpdb}" >> ${currentdir}/job_info.out
        else
            echo "Failed to prepare a PDB file for $PDBID."
            exit 1
        fi
    else
        [[ -f $PDB_FILE ]] || cp ${currentdir}/$PDBFILE .
        preparePDB.py -f $PDB_FILE -c "$CHAINID" -m $MODELID -type "$residuetypes" > prepare.log
        wait
        fixedpdb=`ls *_fixed.pdb`
        if [ -f "$fixedpdb" ]; then
            echo "CpHMD PDB file: ${fixedpdb}" >> ${currentdir}/job_info.out
        else
            echo "Failed to prepare a PDB file for $PDBID."
            exit 1
        fi
    fi
fi
protein=$(echo ${fixedpdb%_fixed.pdb})
echo 'PDB Prepared' > ${currentdir}/job_status.log


######################
# Standard Variables #
######################
conc=$ionic   	# Salt Concentration, Implicent Salt Concentration
crysph=7.0      # Starting pH, can come from crystal structure for protein
cutoff=999.0    # Nonbond cutoff small system 9.0 ang, otherwise use 12.0 ang; for GB, use 999.0
ts=0.002	    # Time step

########################
# Minimization Options #
########################
nsteps=1000      	# Number of total minimization steps
nsdsteps=500     	# Initial amount of total minimization as SD steps
wfrq=50         	# How often to write ene. vel. temp. restart and lamb.
restraint=50.0		# restraints applied on restrainted atoms

###############################################
# Heating NOT Used for Implict Solvent Model  #
###############################################

########################
# Equilibration Options #
########################
etemp=$temperature                      # Equilibration temperature
ensteps=2000                            # Number of each equilibration step
ewfrq=100000                            # How often to write ene. vel. temp. and lamb.
ewrst=100000                            # How often to write restart 
erestraints=(5.0 2.0 1.0 0.0)           # restraints applied on restrainted atoms
ets=$ts

########################
# Production Options #
########################
lfrq=1000                                   # How often to print lambda values for heating, equil., and prod.
pnsteps=$(echo $simlength*1000*500 | bc)    # Total MD steps
pwfrq=10000                                 # How often to write trajectories; for test only
# if [ "$istest" = false ] ; then
#     pwfrq=0                                 # For pKa calculation only in cphmd, don't output trajectories.
# fi
pwrst=10000000000                           # How often to write restart
pts=$ts

# CpHMD phmdin file
IFS=' ' read -ra my_array <<< "$residuetypes"
res=""
for i in "${my_array[@]}"
do
    if [ "$i" == "Asp" ]; then
        res+="'AS2',"
    fi
    if [ "$i" == "Glu" ]; then
        res+="'GL2',"
    fi
    if [ "$i" == "His" ]; then
        res+="'HIP',"
    fi
    if [ "$i" == "Cys" ]; then
        res+="'CYS',"
    fi
    if [ "$i" == "Lys" ]; then
        res+="'LYS',"
    fi
    if [ "$i" == "Tyr" ]; then
    res+="'TYR',"
    fi
done

echo "&phmdin
    QMass_PHMD = 10,                    		! Mass of the Lambda Particle
    Temp_PHMD = 300,                    		! Temp of the lambda particle
    phbeta = 5,                         		! Friction Coefficient (1/ps) for titration integrator
    iphfrq = 1,                         		! Frequency to update the lambda forces
    NPrint_PHMD = $lfrq,                		! How often to print the lambda
    PrLam = .true.,                     		! Should lambda be printed
    PrDeriv = .false.,                  		! Do you want to print the derivatives?
    PRNLEV = 7,                         		! Sets what is printed out, (7) normal print level
    PHTest = 0,                         		! Are lambda and theta fixed, (0) = not fixed, (1) = fixed
    MaskTitrRes(:) = $res 	                    ! Residues to include as titratable
    MaskTitrResTypes = ${#my_array[@]},                       ! number of titratable types
    QPHMDStart = .true.,                		! Initialize velocities of titration varibles with a Boltzmann distribution
    /" > ${protein}.phmdin 
echo "CpHMD phmdin file: ${protein}.phmdin" >> ${currentdir}/job_info.out
cp $cphmd/Files/gbneck2_input.parm .
inputparm='gbneck2_input.parm'
echo "CpHMD parameter file: gbneck2_input.parm" >> ${currentdir}/job_info.out

############################################
# Let tleap prepare Amber inputs           #
############################################
awk -F '' '$14 !~ /^(H)$/' ${protein}_fixed.pdb > tmp.pdb
awk -F '' '$13 !~ /^(H)$/' tmp.pdb > ${protein}_fixed.pdb
# Make build.in file
echo "# Reads in the FF
    source $cphmd/Files/leaprc.protein.ff14SB     			    # Load protein
    set default PBradii mbondi3                             	# Modifies GB radii to mbondi3 set, needed for IGB8 this is the version of the GB
    loadOff $cphmd/Files/phmd.lib                               # load modified library file, loads definitions for AS2 GL2 (ASPP AND GLUPP)
    phmdparm = loadamberparams $cphmd/Files/frcmod.phmd        	# load modifications to bond length and dihedrals
    $protein = loadPDB ${protein}_fixed.pdb                            	# Load PDB
    saveamberparm $protein $protein.parm7 $protein.rst7         # generates the parameter files and coords file
    savepdb $protein $protein.pdb_tmp                           # save the pdb file with right atom numbering scheme wrt the parm7 file
    quit
" > build.in
   
# Run tleap
$amberbin/tleap -f build.in > tleap.log
wait    
# Change the radii of SG in Cys and HD1/HE2 in HIP using processParm7.py
# Interaction between HD1(HE1) and HD2(HE2) dummy hydrogens in AS2/GL2 is also added to the exclusion list
processParm7.py -f $protein.pdb_tmp -p $protein.parm7
mv $protein.parm7 orig_$protein.parm7
cp cphmd_$protein.parm7 $protein.parm7
wait
mv $protein.pdb_tmp $protein.pdb
echo "Amber topology rst7 file: $protein.rst7" >> ${currentdir}/job_info.out
echo "Amber parameter parm7 file: $protein.parm7" >> ${currentdir}/job_info.out

echo 'Amber inputs Prepared' > ${currentdir}/job_status.log

################################
#  Prepare Minimization Inputs #
################################
# Generate $protein_mini.mdin
nres=$(grep "FLAG POINTERS" -A 3 ${protein}.parm7 | tail -n 1 | awk '{print $2}')
natom=$(tail $protein.pdb | grep 'HN2 NHE' | awk '{print $2}')
nrestr=$(($nres - 2)) # We don't restrain the end residues.
# Check Amber version, because < Amber 20 cannot use pmemd.cuda for minimization
version=$($AMBERHOME/bin/pmemd.cuda --version | awk '{print $(NF)}')
amber20='20.0'
if [ "$natom" -le 1000 ];then
    np=4
elif [ "$natom" -le 100 ];then
    np=2
else
    np=$(grep -c ^processor /proc/cpuinfo)
fi
echo "Minimization
    &cntrl                                         ! cntrl is a name list, it stores all conrol parameters for amber
    imin = 1, maxcyc = $nsteps, ncyc = $nsdsteps, ! Do minimization, max number of steps (Run both SD and Conjugate Gradient), first 250 steps are SD
    ntx = 1,                                     ! Initial coordinates
    ntwe = 0, ntwr = $nsteps, ntpr = $wfrq,        ! Print frq for energy and temp to mden file, write frq for restart trj, print frq for energy to mdout 
    ntc = 1, ntf = 1, ntb = 0, ntp = 0,          ! Shake (1 = No Shake), Force Eval. (1 = complete interaction is calced), Use PBC (1 = const. vol.), Use Const. Press. (0 = no press. scaling)
    cut = $cutoff,                               ! Nonbond cutoff (Ang.)
    ntr = 1, restraintmask = ':3-$nrestr&!@H=',    ! restraint atoms (1 = yes), Which atoms are restrained 
    restraint_wt = $restraint,                   ! Harmonic force to be applied as the restraint
    ioutfm = 1, ntxo = 2,                        ! Fomrat of coor. and vel. trj files, write NetCDF restrt fuile for final coor., vel., and box size
    igb = 8,
    /" > ${protein}_mini.mdin 
if (( $(echo "$version >= $amber20" | bc) )); then
    $amberbin/pmemd.cuda -O -i ${protein}_mini.mdin -c ${protein}.rst7 -p ${protein}.parm7 -ref ${protein}.rst7 -r ${protein}_mini.rst7 -o ${protein}_mini.out &>> mini.log
else
    mpirun -n $np $amberbin/pmemd.MPI -O -i ${protein}_mini.mdin -c ${protein}.rst7 -p ${protein}.parm7 -ref ${protein}.rst7 -r ${protein}_mini.rst7 -o ${protein}_mini.out &>> mini.log
fi 

echo "Minimization input file: ${protein}_mini.mdin" >> ${currentdir}/job_info.out
echo 'System minimized' > ${currentdir}/job_status.log    
    
#############################
#  Perform Equilibration #
#############################
# Generate the right titratable residue types for Amber CpHMD
echo 'Equilibrate system...' > ${currentdir}/job_status.log

# Create input files for equilibration
for restn in `seq 1 ${#erestraints[@]}` # loop over number of restraints
do
    echo "Equilibration step $restn" > ${currentdir}/job_status.log
    if [ $restn == 1 ]; then
        echo "Stage $restn equilibration of $protein 
            &cntrl
            imin = 0, nstlim = $ensteps, dt = $ets,
            irest = 0, ntx = 1,ig = -1,
            temp0 = $etemp,
            ntc = 2, ntf = 2, tol = 0.00001,
            ntwx = $ewfrq, ntwe = $ewfrq, ntwr = $ewrst, ntpr = $ewfrq,
            cut = $cutoff, iwrap = 0, igb = 8,           
            ntt = 3, gamma_ln = 1.0, ntb = 0,                               ! ntp (1 = isotropic position scaling)
            iphmd = 1, solvph = $crysph, saltcon = $conc,
            ntr = 1, restraintmask = ':1-${nres}&!@H=',
            restraint_wt = ${erestraints[$(($restn-1))]},
            ioutfm = 1, ntxo = 2,                                           ! Fomrat of coor. and vel. trj files, write NetCDF restrt fuile for final coor., vel., and box size
            /" > ${protein}_equil${restn}.mdin
        echo "Equilibration step $restn input file: ${protein}_equil${restn}.mdin" >> ${currentdir}/job_info.out
        equilstart="${protein}_mini.rst7"
        $amberbin/pmemd.cuda -O -i ${protein}_equil${restn}.mdin -c $equilstart -p ${protein}.parm7 -ref $equilstart -r ${protein}_equil${restn}.rst7 \
            -o ${protein}_equil${restn}.out -x ${protein}_equil${restn}.nc -phmdin ${protein}.phmdin -phmdparm $inputparm -phmdout ${protein}_equil${restn}.lambda  -phmdrestrt ${protein}_equil${restn}.phmdrst &>> equil.log
        wait
    else
        echo "Stage $restn equilibration of $protein 
            &cntrl
            imin = 0, nstlim = $ensteps, dt = $ets,
            irest = 1, ntx = 5,ig = -1,
            temp0 = $etemp,
            ntc = 2, ntf = 2, tol = 0.00001,
            ntwx = $ewfrq, ntwe = $ewfrq, ntwr = $ewrst, ntpr = $ewfrq,
            cut = $cutoff, iwrap = 0, igb = 8,           
            ntt = 3, gamma_ln = 1.0, ntb = 0,                               ! ntp (1 = isotropic position scaling)
            iphmd = 1, solvph = $crysph, saltcon = $conc,
            ntr = 1, restraintmask = ':1-${nres}&!@H=',
            restraint_wt = ${erestraints[$(($restn-1))]},
            ioutfm = 1, ntxo = 2,                                           ! Fomrat of coor. and vel. trj files, write NetCDF restrt fuile for final coor., vel., and box size
            /" > ${protein}_equil${restn}.mdin
        echo "Equilibration step $restn input file: ${protein}_equil${restn}.mdin" >> ${currentdir}/job_info.out
        sed -i 's/QPHMDStart = .true.,/QPHMDStart = .false.,/g' ${protein}.phmdin
        prev=$(($restn-1))
        equilstart="${protein}_equil${prev}.rst7"
        phmdstart="${protein}_equil${prev}.phmdrst"
        sed -i 's/PHMDRST/PHMDSTRT/g' $phmdstart
        $amberbin/pmemd.cuda -O -i ${protein}_equil${restn}.mdin -c $equilstart -p ${protein}.parm7 -ref $equilstart -r ${protein}_equil${restn}.rst7 \
            -o ${protein}_equil${restn}.out -x ${protein}_equil${restn}.nc -phmdin ${protein}.phmdin -phmdparm $inputparm \
            -phmdstrt $phmdstart -phmdout ${protein}_equil${restn}.lambda -phmdrestrt ${protein}_equil${restn}.phmdrst &>> equil.log
    fi
done
echo 'System equilibrated' > ${currentdir}/job_status.log

#############################
#  Perform Production #
#############################
# Create input files for Production 
final_equil_stage=${#erestraints[@]}   
sed -i 's/PHMDRST/PHMDSTRT/g' ${protein}_equil${final_equil_stage}.phmdrst 
visible_gpus=`nvidia-smi -q | grep 'Minor Number'| rev | cut -c1 |tr "\n" ","`  # for CUDA_VISIBLE_DEVICES string
gpu_list=(`nvidia-smi -q | grep 'Minor Number'| rev | cut -c1`)     # for listing the CUDA_VISIBLE_DEVICES
cp $cphmd/cphmd_tools/pkaCal.py .

if [ "$mode" == "reex" ]; then    # Use Replica Exchange
    numexchg=$(echo $simlength*500 | bc)
    rm -rf cphmd.groupfile
    for rep in "${!pHs[@]}"
    do
        echo "Begin pH replica exchange CpHMD production at pHs: ${pHs[@]}" > ${currentdir}/job_status.log
        echo "Production stage of $protein using Replica Exchange
            &cntrl
            imin = 0, nstlim = $lfrq, dt = $pts, numexchg=$numexchg,
            irest = 1, ntx = 5, ig = -1, 
            tempi = $etemp, temp0 = $etemp, 
            ntc = 2, ntf = 2, tol = 0.00001,
            ntwx = $pwfrq, ntwe = $pwfrq, ntwr = $pwfrq, ntpr = 500, 
            cut = $cutoff, iwrap = 0, igb = 8,
            ntt = 3, gamma_ln = 1.0, ntb = 0,
            iphmd = 1, solvph= ${pHs[$rep]}, saltcon = $conc,
            ioutfm = 1, ntxo = 2,                        
            /" > rep${rep}.mdin
            /usr/bin/printf "-O -i rep${rep}.mdin -c ${protein}_equil${final_equil_stage}.rst7 -p ${protein}.parm7 \
                -phmdin ${protein}.phmdin -phmdparm $inputparm -phmdstrt ${protein}_equil${final_equil_stage}.phmdrst \
                -phmdout rep${rep}.lambda -phmdrestrt rep${rep}.phmdrst -o rep${rep}.mdout -r rep${rep}.rst7 \
                -x rep.nc.%03d -rem 4 -remlog rem${rep}.log -inf pH${pHs[$rep]}.mdinfo \n" $rep  >> cphmd.groupfile
    done
    echo "pH replica exchange input file/groupfile: cphmd.groupfile" >> ${currentdir}/job_info.out
    mpirun="mpirun --leave-session-attached"
    # TODO: test this; until done, don't use this method
    $mpirun --allow-run-as-root -np $num_gpus $amberbin/pmemd.MPI -ng ${#pHs[@]} -groupfile cphmd.groupfile &>> prod.log 
    echo "CpHMD Done" > ${currentdir}/job_status.log
    # Calculate pKa's 
    # TODO: convert replica exchange lambda files to pH lambda files.
    # python pkaCal.py -id $PDB_ID -cp $protein.pdb -c "$CHAINID" -t "$residuetypes" > pkaCal.log
    echo "pKa calculated" > ${currentdir}/job_status.log
elif [ "$mode" == "independent" ]; then      # Don't use Replica Exchange
    echo "Begin independent pH CpHMD production at pHs: ${pHs[@]}" > ${currentdir}/job_status.log
    for pH in "${pHs[@]}"
    do
        echo "Production stage of $protein
            &cntrl
            imin = 0, nstlim = $pnsteps, dt = $pts, 
            irest = 1, ntx = 5, ig = -1, 
            tempi = $etemp, temp0 = $etemp, 
            ntc = 2, ntf = 2, tol = 0.00001,
            ntwx = $pwfrq, ntwe = $pwfrq, ntwr = $pwfrq, ntpr = 500, 
            cut = $cutoff, iwrap = 0, igb = 8,
            ntt = 3, gamma_ln = 1.0, ntb = 0,
            iphmd = 1, solvph= $pH, saltcon = $conc,
            ioutfm = 1, ntxo = 2,                        
            /" > ${protein}_pH${pH}.mdin
    done
    echo "pH $pH production input file: ${protein}_pH${pH}.mdin" >> ${currentdir}/job_info.out

    # Run productions on all available GPUs in parrallel
    # init=0
    avail_gpus=$num_gpus
    if [ $num_gpus -lt ${#pHs[@]} ]; then   # number of pHs is larger than number of gpus
        for pH in "${pHs[@]}"
            # find an idle gpu
        do
            wait=true
            while [ "$avail_gpus" -gt 0 ] && [ "$wait" = true ]  # submit as many jobs as number of GPUs
            do
                for gpu in "${gpu_list[@]}"
                do
                    gpu_util=`nvidia-smi -i $gpu -q -d UTILIZATION | grep 'Gpu' |awk '{print $3}'`
                    if [ "$gpu_util" -lt 20 ]; then     # idle gpu
                        export CUDA_VISIBLE_DEVICES=$gpu
                        $amberbin/pmemd.cuda -O -i ${protein}_pH${pH}.mdin -c ${protein}_equil${final_equil_stage}.rst7 -p ${protein}.parm7 \
                            -r ${protein}_pH${pH}_prod1.rst7 -o ${protein}_pH${pH}_prod1.out -x ${protein}_pH${pH}_prod1.nc \
                            -phmdin ${protein}.phmdin -phmdparm $inputparm -phmdstrt ${protein}_equil${final_equil_stage}.phmdrst \
                            -phmdout ${protein}_pH${pH}_prod1.lambda -phmdrestrt ${protein}_pH${pH}_prod1.phmdrst -inf pH${pH}.mdinfo \
                            &>> prod.log &
                        echo "pH=${pH} is running on GPU $gpu" >> ${currentdir}/job_status.log
                        sleep 2s
                        avail_gpus=$((avail_gpus-1))
                        wait=false
                        break
                    fi
                done
            done
            while [ "$avail_gpus" -eq 0 ]   # wait for one idle GPU
            do
                for gpu in "${gpu_list[@]}"
                do 
                    gpu_util=`nvidia-smi -i $gpu -q -d UTILIZATION | grep 'Gpu' |awk '{print $3}'`
                    if [ "$gpu_util" -lt 20 ]; then     # new idle gpu
                        avail_gpus=$((avail_gpus+1))
                        echo "Job on GPU $gpu is done" >> ${currentdir}/job_status.log
                    fi
                done
                if [ "$avail_gpus" -eq 0 ]; then
                    sleep 10s
                fi
            done
        done
        wait
        # while [ $init -lt ${#pHs[@]} ]
        #     do
        #         s_pHs=("${pHs[@]:$init:$num_gpus}")
        #         echo "Begin CpHMD production at pHs: ${s_pHs[@]}" > ${currentdir}/job_status.log
        #         for i in "${!s_pHs[@]}"
        #             do
        #                 export CUDA_VISIBLE_DEVICES=$i
        #                 pH=${s_pHs[$i]}
        #                 $amberbin/pmemd.cuda -O -i ${protein}_pH${pH}.mdin -c ${protein}_equil${final_equil_stage}.rst7 -p ${protein}.parm7 \
        #                     -r ${protein}_pH${pH}_prod1.rst7 -o ${protein}_pH${pH}_prod1.out -x ${protein}_pH${pH}_prod1.nc \
        #                     -phmdin ${protein}.phmdin -phmdparm $inputparm -phmdstrt ${protein}_equil${final_equil_stage}.phmdrst \
        #                     -phmdout ${protein}_pH${pH}_prod1.lambda -phmdrestrt ${protein}_pH${pH}_prod1.phmdrst -inf pH${pH}.mdinfo \
        #                     &>> prod.log &
        #                 echo "pH=${pH} is running on GPU $i" >> ${currentdir}/job_status.log
        #             done
        #         wait 
        #         echo "pH = [${s_pHs[@]}] are done" >> ${currentdir}/job_status.log
        #         init=$((init + num_gpus))
        #     done
    else  # number of gpus is larger than number of pHs
        for i in "${!pHs[@]}"
        do
            export CUDA_VISIBLE_DEVICES=$i
            pH=${pHs[$i]}
            $amberbin/pmemd.cuda -O -i ${protein}_pH${pH}.mdin -c ${protein}_equil${final_equil_stage}.rst7 -p ${protein}.parm7 \
                -r ${protein}_pH${pH}_prod1.rst7 -o ${protein}_pH${pH}_prod1.out -x ${protein}_pH${pH}_prod1.nc \
                -phmdin ${protein}.phmdin -phmdparm $inputparm -phmdstrt ${protein}_equil${final_equil_stage}.phmdrst \
                -phmdout ${protein}_pH${pH}_prod1.lambda -phmdrestrt ${protein}_pH${pH}_prod1.phmdrst -inf pH${pH}.mdinfo \
                &>> prod.log &
            echo "pH=${pH} is running on GPU $i" >> ${currentdir}/job_status.log
        done
        wait 
        echo "pH = [${pHs[@]}] are done" > ${currentdir}/job_status.log
    fi
    echo "CpHMD Done" > ${currentdir}/job_status.log
    # Calculate pKa's 
    python pKaCal.py -id $PDB_ID -cp $protein.pdb -c "$CHAINID" -t "$residuetypes" > pKaCal.log
    echo "pKa calculated" > ${currentdir}/job_status.log
elif [ "$mode" == "async" ]; then
    rexsteps=$(echo $pnsteps/1000 | bc)
    echo "Begin asynchronous pH replica exchange CpHMD for $rexsteps swaps" > ${currentdir}/job_status.log
    echo "Asynchronous pH replica exchange swap steps: $rexsteps" >> ${currentdir}/job_info.out
    echo "Production stage of $protein
        &cntrl
        imin = 0, nstlim = 1000, dt = $pts, 
        irest = 1, ntx = 5, ig = -1, 
        tempi = $etemp, temp0 = $etemp, 
        ntc = 2, ntf = 2, tol = 0.00001,
        ntwx = $pwfrq, ntwe = 0, ntwr = 0, ntpr = 0, 
        cut = $cutoff, iwrap = 0, igb = 8,
        ntt = 3, gamma_ln = 1.0, ntb = 0,
        iphmd = 1, solvph= pH, saltcon = $conc,
        ioutfm = 1, ntxo = 2,                        
    /" > template_${protein}.mdin
    echo "Asynchronous pH replica exchange template input file: template_${protein}.mdin" >> ${currentdir}/job_info.out
    cp $cphmd/cphmd_tools/apHREX.py .
    cp $cphmd/Files/gbneck2_input.parm .
    python apHREX.py $rexsteps template_${protein}.mdin ${protein}.parm7 gbneck2_input.parm ${protein}.phmdin ${protein}_cphmd ${protein}_equil${final_equil_stage}.rst7 "${pHs[@]}" > async.log 2>&1 &
    # Monitor the progress
    sleep 1s
    first_swap=`tail -1 ${protein}_cphmd_0/reps.log | awk '{print $2}'`
    current_swap=`tail -1 ${protein}_cphmd_0/reps.log | awk '{print $2}'`
    diff=`echo "$current_swap - $first_swap" | bc`
    while [[ $diff -lt 11 ]]
    do
            sleep 0.1
            current_swap=`tail -1 ${protein}_cphmd_0/reps.log | awk '{print $2}'`
            diff=`echo "$current_swap - $first_swap" | bc`
            if [[ $diff -eq 1 ]]; then
                    start=`date +%s.%N`
            fi
    done
    end=`date +%s.%N`
    time_per_step=`echo "($end - $start) * 0.1" | bc`
    remain=`expr $rexsteps - $current_swap`
    echo "$remain * $time_per_step" | bc > ${currentdir}/job_status.log
    rexsteps=`echo "$rexsteps - 1" | bc`  # it's 0-based in the reps.log file
    while [[ $current_swap -lt $rexsteps ]]
    do
            current_swap=`tail -1 ${protein}_cphmd_0/reps.log | awk '{print $2}'`
            remain=`echo "$rexsteps - $current_swap" | bc`
            echo "$remain * $time_per_step" | bc > ${currentdir}/job_status.log
            sleep 5s
    done
    echo "CpHMD Done" > ${currentdir}/job_status.log
    # Convert lambda files
    rm -rf ${protein}*prod.lambda || true
    for i in `seq 1 ${#pHs[@]}`
    do
        rep=$(($i-1))
        ph=${pHs[$(($i-1))]}
        cat ${protein}_cphmd_0/rep${rep}.lambda >> ${protein}_pH${ph}_prod.lambda
    done
    # Calculate pKa's
    python pkaCal.py -id $PDB_ID -cp $protein.pdb -c "$CHAINID" -t "$residuetypes" -f "${protein}_pH*_prod.lambda" > pKaCal.log
    echo "pKa calculated" > ${currentdir}/job_status.log
fi

#############################
#  Clean up files   #
#############################
if [ "$istest" = false ]; then
    rm tmp1.pdb tmp2.pdb *_tmp.pdb tmp*.parm7 2> /dev/null
    rm tleap.log mden mdinfo leap.log prepare.log build.in 2> /dev/null
fi
# else

#     ###############################
#     #  Perform Production Restart #
#     ###############################
#     echo 'Begin CpHMD restart' > ${currentdir}/job_status.log

#     # Prepare restart files
#     for pH in "${pHs[@]}"
#         do
#             lastrst7=`ls -t ${protein}_pH${pH}_prod*.rst7|head -1`
#             prev_stage=$(echo $lastrst7 |sed -e 's/.*prod\(.*\).rst7/\1/')
#             stage=$(($prev_stage + 1))
#             sed -i 's/PHMDRST/PHMDSTRT/g' ${protein}_pH${pH}_prod${prev_stage}.phmdrst
#         done
    
#     # Run restart
#     init=0
#     if [ $num_gpus -lt ${#pHs[@]} ]; then
#         while [ $init -lt ${#pHs[@]} ]
#             do
#                 s_pHs=("${pHs[@]:$init:$num_gpus}")
#                 echo "Begin CpHMD restart at pHs: ${s_pHs[@]}" >> ${currentdir}/job_status.log
#                 for i in "${!s_pHs[@]}"
#                     do
#                         export CUDA_VISIBLE_DEVICES=$i
#                         pH=${s_pHs[$i]}
#                         $amberbin/pmemd.cuda -O -i ${protein}_pH${pH}.mdin -c ${protein}_pH${pH}_prod${prev_stage}.rst7 -p ${protein}.parm7 \
#                             -r ${protein}_pH${pH}_prod${stage}.rst7 -o ${protein}_pH${pH}_prod${stage}.out -x ${protein}_pH${pH}_prod${stage}.nc \
#                             -phmdin ${protein}.phmdin -phmdparm $inputparm -phmdstrt ${protein}_pH${pH}_prod${prev_stage}.phmdrst \
#                             -phmdout ${protein}_pH${pH}_prod${stage}.lambda -phmdrestrt ${protein}_pH${pH}_prod${stage}.phmdrst -inf pH${pH}.mdinfo \
#                             &>> restart.log &
#                         echo "pH=${pH} is running on GPU $i" >> ${currentdir}/job_status.log
#                     done
#                 wait 
#                 echo "pH = [${s_pHs[@]}] are done" >> ${currentdir}/job_status.log
#                 init=$((init + num_gpus))
#             done
#     else
#         echo "Begin CpHMD restart at pHs: ${pH[@]}" > ${currentdir}/job_status.log
#         for i in "${!pHs[@]}"
#             do
#                 export CUDA_VISIBLE_DEVICES=$i
#                 pH=${pHs[$i]}
#                 $amberbin/pmemd.cuda -O -i ${protein}_pH${pH}.mdin -c ${protein}_pH${pH}_prod${prev_stage}.rst7 -p ${protein}.parm7 \
#                     -r ${protein}_pH${pH}_prod${stage}.rst7 -o ${protein}_pH${pH}_prod${stage}.out -x ${protein}_pH${pH}_prod${stage}.nc \
#                     -phmdin ${protein}.phmdin -phmdparm $inputparm -phmdstrt ${protein}_pH${pH}_prod${prev_stage}.phmdrst \
#                     -phmdout ${protein}_pH${pH}_prod${stage}.lambda -phmdrestrt ${protein}_pH${pH}_prod${stage}.phmdrst -inf pH${pH}.mdinfo \
#                     &>> restart.log &
#                 echo "pH=${pH} is running on GPU $i" >> ${currentdir}/job_status.log
#             done
#         wait 
#         echo "pH = [${pHs[@]}] are done" > ${currentdir}/job_status.log
#     fi
#     echo "CpHMD Restart Done" > ${currentdir}/job_status.log
# fi

# 

if [ "$outfiletype" == 'zip' ]; then
    zip -r cphmd_results.zip *png *.lambda *.data pKa.csv ${protein}.pdb >/dev/null 2>&1
    mv cphmd_results.zip ../results.zip 
    echo "CpHMD results are in: cphmd_results.zip" >> ${currentdir}/job_info.out
else
    tar -czvf cphmd_results.tar.gz *png *.lambda *.data pKa.csv ${protein}.pdb  >/dev/null 2>&1 
    mv cphmd_results.tar.gz ../cphmd_results.tar.gz
    echo "CpHMD results are in: cphmd_results.tar.gz" >> ${currentdir}/job_info.out
fi
echo "Results file generated" > ${currentdir}/job_status.log
sleep 1s
echo "Done" > ${currentdir}/job_status.log
exit
